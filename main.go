package main

import (
	"bufio"
	"bytes"
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"net"
	"net/http"
	"net/textproto"
	"os"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"bitbucket.org/modima/sipproxy/siprocket"
	"bitbucket.org/modima/sipproxy/ttlcache"
	"github.com/op/go-logging"
)

const (
	VERSION      = "2.0.6"
	LOGLEVEL     = 3
	WORKER_COUNT = 50
	BASE_URL     = "https://api.dialfire.com"
	//BASE_URL = "https://devsystem.dialfire.com"
)

var (
	debugMode bool
	tenantID  string
	publicIP  string
	port      string
	udpCon    net.PacketConn
	log       = logging.MustGetLogger("sipProxy")
	logFormat = logging.MustStringFormatter(
		`%{color}%{time:2006/01/02 15:04:05.000} %{shortfunc} %{level:.4s} â–¶ %{color:reset} %{message}`,
	)
)

/******************************************
* UTILITY FUNCTIONS
*******************************************/

func createDirectory(path string) error {

	if _, err := os.Stat(path); err != nil {

		if os.IsNotExist(err) {

			err = os.MkdirAll(path, 0755)
			if err != nil {
				return err
			}
		} else {
			return err
		}
	}
	return nil
}

func initLogger(filePath string, logLevel int) error {

	var logBackend logging.Backend
	if debugMode {

		fmt.Printf("Logfile: stdout\n")
		logBackend = logging.NewLogBackend(os.Stdout, "", 0)
	} else {

		var dirPath = filePath[:strings.LastIndex(filePath, "/")]
		if err := createDirectory(dirPath); err != nil {
			return err
		}

		logFile, err := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			return err
		}

		fmt.Printf("Logfile: %v\n", filePath)
		logBackend = logging.NewLogBackend(logFile, "", 0)
	}

	logBackendFormatter := logging.NewBackendFormatter(logBackend, logFormat)
	logBackendLeveled := logging.AddModuleLevel(logBackendFormatter)
	switch logLevel {
	case 0:
		logBackendLeveled.SetLevel(logging.CRITICAL, "")
	case 1:
		logBackendLeveled.SetLevel(logging.ERROR, "")
	case 2:
		logBackendLeveled.SetLevel(logging.WARNING, "")
	case 3:
		logBackendLeveled.SetLevel(logging.NOTICE, "")
	case 4:
		logBackendLeveled.SetLevel(logging.INFO, "")
	case 5:
		logBackendLeveled.SetLevel(logging.DEBUG, "")
	}
	logging.SetBackend(logBackendLeveled)

	return nil
}

func _main() {
	f, _ := os.Open("./test.sip")
	b := make([]byte, 4096)
	f.Read(b)
	msg := parseMessage(b)

	fmt.Printf("%s\n", msg.RequestLine.Method)

	fmt.Printf("%s", msg.serialize())
}

func main() {

	flag.Usage = func() {
		var description = `sipProxy v` + VERSION
		fmt.Printf("Usage of %v\n", description)
		fmt.Printf("Command line options:\n")
		flag.PrintDefaults()
		os.Exit(0)
	}

	// parse cli arguments
	listenIP := flag.String("i", "0.0.0.0", "IP of the network interface this proxy should listen to")
	listenPort := flag.String("p", "5060", "Network port this proxy should listen to")
	ip := flag.String("ip", "", "Public IP adress of this proxy")
	logLevel := flag.Int("l", LOGLEVEL, "Log level (0 - CRITICAL .. 5 - DEBUG")
	logDir := flag.String("ld", "/var/log/sipProxy/", "Directory for logfiles")
	tenant := flag.String("t", "", "ID of the tenant to which this proxy is dedicated to (all - accept all calls WITH authorization | open - accept all calls WITHOUT authorization)")
	debug := flag.Bool("d", false, "Debug mode (Set log level to 5 and print all log messages to stdout instead of logfile)")

	flag.Parse()

	if *ip != "" {
		publicIP = *ip
	} else {
		// Destination IP cannot be retrieved from UDP Packets in golang --> so it must be set explicitly
		if *listenIP == "0.0.0.0" {
			log.Fatal("At least one of parameters i or ip is required")
		}
		publicIP = *listenIP
	}
	port = *listenPort

	if *tenant == "" {
		log.Fatal("Parameter 't' is required")
	} else {
		tenantID = *tenant
	}

	debugMode = *debug
	if debugMode {
		*logLevel = 5
	}

	// create logger
	var err error
	err = initLogger(*logDir+time.Now().Format("20060102150405")+".log", *logLevel)
	if err != nil {
		err = initLogger(os.Getenv("HOME")+"/.sipProxy/log/"+time.Now().Format("20060102150405")+".log", *logLevel)
		if err != nil {
			log.Fatal(err.Error())
		}
	}

	// listen to incoming udp packets
	udpCon, err = net.ListenPacket("udp", *listenIP+":"+*listenPort)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer udpCon.Close()

	// start worker
	go bufferMonitor()
	for i := 0; i < WORKER_COUNT; i++ {
		go registrationStarter()
		go registrationChallengeHandler()
		go messageParser()
		go messageSender()
		go messageReceiver()
		go messageProcessor(i)
	}

	// wait forever
	var wg sync.WaitGroup
	wg.Add(1)
	wg.Wait()
}

func bufferMonitor() {
	for {
		log.Infof("UDP SEND: %d | UDP RECV: %d | SIP PARSER: %d | SIP PROCESSOR: %d", atomic.LoadInt32(&cntSIPMessagesToSend), atomic.LoadInt32(&cntReceivedUDPPackets), atomic.LoadInt32(&cntUDPPacketsToParse), atomic.LoadInt32(&cntSIPMessagesToProcess))
		time.Sleep(time.Second * 10)
	}
}

var chanOutboundMessages = make(chan *SIPMessage)
var cntSIPMessagesToSend int32

func messageSender() {

	for {

		msg := <-chanOutboundMessages
		atomic.AddInt32(&cntSIPMessagesToSend, 1)

		//log.Debugf("send SIP message: \n%s\n", msg.serialize())

		var uri string
		if msg.Type == MSG_TYPE_REQUEST {

			if len(msg.Headers["Route"]) > 0 {
				// 1st try build uri from top most route header
				var route = siprocket.SipRoute{}
				siprocket.ParseSipRoute([]byte(msg.Headers["Route"][0]), &route)
				//log.Debugf("Parsed route: %v", route)
				uri = string(route.Host)
				var splits = strings.Split(uri, "@")
				uri = splits[len(splits)-1]
				if len(route.Port) > 0 {
					uri += ":" + string(route.Port)
				} else {
					uri += ":5060"
				}
			} else {
				// 2nd try build uri from request uri
				uri = string(msg.RequestLine.Host)
				if len(msg.RequestLine.Port) > 0 {
					uri += ":" + string(msg.RequestLine.Port)
				} else {
					uri += ":5060"
				}
			}

		} else if msg.Type == MSG_TYPE_RESPONSE {

			if len(msg.Headers["Via"]) == 0 {
				log.Infof("Discard message (no via)")
				atomic.AddInt32(&cntSIPMessagesToSend, -1)
				continue
			}

			// build uri from top most via header
			var via = siprocket.SipVia{}
			siprocket.ParseSipVia([]byte(msg.Headers["Via"][0]), &via)
			uri = string(via.Host)
			if len(via.Port) > 0 {
				uri += ":" + string(via.Port)
			} else {
				uri += ":5060"
			}
		}

		udpAddr, err := net.ResolveUDPAddr("udp", uri)
		if err != nil {
			log.Errorf(err.Error())
			atomic.AddInt32(&cntSIPMessagesToSend, -1)
			continue
		}

		if udpAddr.IP.String() == "127.0.0.1" || udpAddr.IP.String() == msg.IP {
			log.Infof("invalid ip: %v", udpAddr.IP.String())
			atomic.AddInt32(&cntSIPMessagesToSend, -1)
			continue
		}

		udpPacket := msg.serialize()

		log.Debugf("send message to udp/[%v]:%v:\n", udpAddr.IP, udpAddr.Port)

		n, err := udpCon.WriteTo(udpPacket, udpAddr)
		if err != nil {
			log.Errorf(err.Error())
			atomic.AddInt32(&cntSIPMessagesToSend, -1)
			continue
		}

		log.Infof("send %v bytes to udp/[%v]:%v:\n----------------------------------------------------------------------------------\n%s----------------------------------------------------------------------------------\n", n, udpAddr.IP, udpAddr.Port, udpPacket)

		atomic.AddInt32(&cntSIPMessagesToSend, -1)
	}
}

type UDPPacket struct {
	LocalIP  string
	RemoteIP string
	Payload  []byte
}

var chanUDPPacket = make(chan UDPPacket)
var cntReceivedUDPPackets int32

func messageReceiver() {

	for {
		buf := make([]byte, 4096)
		n, remoteAddr, err := udpCon.ReadFrom(buf)
		localAddr := udpCon.LocalAddr()
		log.Debugf("Local ADDR: %v", localAddr)
		if err != nil {
			log.Errorf(err.Error())
			continue
		}
		buf = buf[:n]
		log.Infof("received %v bytes from udp/%v:\n----------------------------------------------------------------------------------\n%s\n----------------------------------------------------------------------------------\n", n, remoteAddr.String(), buf)

		atomic.AddInt32(&cntReceivedUDPPackets, 1)
		chanUDPPacket <- UDPPacket{
			RemoteIP: remoteAddr.String(),
			LocalIP:  localAddr.String(),
			Payload:  buf,
		}
		atomic.AddInt32(&cntReceivedUDPPackets, -1)
	}
}

var chanInboundMessage = make(chan *SIPMessage)
var cntUDPPacketsToParse int32

func messageParser() {

	for {
		udpPacket := <-chanUDPPacket
		atomic.AddInt32(&cntUDPPacketsToParse, 1)

		//log.Debugf("parse udp packet: \n%s\n", udpPacket)

		msg := parseMessage(udpPacket.Payload)
		if msg == nil { // seems not to be a valid sip message
			log.Errorf("Invalid sip message: %s", udpPacket)
			atomic.AddInt32(&cntUDPPacketsToParse, -1)
			continue
		}

		chanInboundMessage <- msg
		atomic.AddInt32(&cntUDPPacketsToParse, -1)
	}
}

type RegisterCallParams struct {
	CallID        string
	CSeq          string
	From          string
	Via           string
	Authorization string
}

type RegisterChallenge struct {
	Realm     string
	Nonce     string
	Opaque    string
	Algorithm string
	QOP       string
	NC        int
}

type RegisterParams struct {
	Account        string
	Host           string
	Port           string
	Expires        string
	SignatureToken string
	CallParams     *RegisterCallParams
	Challenge      *RegisterChallenge
}

var chanRegisterStart = make(chan *RegisterParams)

var signatureTokens = ttlcache.NewCache(3 * time.Hour)

//var signatureTokens = ttlcache.NewCache(time.Minute) // Testing

func registrationStarter() {

	for {
		registerParams := <-chanRegisterStart
		//log.Debugf("signatureTokens: %v", signatureTokens)
		if len(registerParams.SignatureToken) > 0 {
			key := fmt.Sprintf("%s@%s", registerParams.Account, registerParams.Host)
			signatureTokens.Set(key, registerParams.SignatureToken)
		}
		key := fmt.Sprintf("%s@%s", registerParams.Account, registerParams.Host)
		if _, ok := signatureTokens.Get(key); ok {
			log.Infof("Start registration for account %s", key)
			sendRegister(registerParams)
		} else {
			log.Infof("Registration for account %s expired", key)
		}
	}
}

var chanRegisterChallenge = make(chan *RegisterParams)

func registrationChallengeHandler() {

	for {
		registerParams := <-chanRegisterChallenge
		key := fmt.Sprintf("%s@%s", registerParams.Account, registerParams.Host)
		if signatureToken, ok := signatureTokens.Get(key); ok {
			//log.Debugf("token: %v", signatureToken)
			var signatureResult = CreateSignature(
				SignatureData{
					Method:    "REGISTER",
					SipURI:    fmt.Sprintf("sip:%s", registerParams.Host),
					Realm:     registerParams.Challenge.Realm,
					Nonce:     registerParams.Challenge.Nonce,
					QOP:       registerParams.Challenge.QOP,
					NC:        1,
					Opaque:    registerParams.Challenge.Opaque,
					Algorithm: registerParams.Challenge.Algorithm,
				}, signatureToken)

			if signatureResult != nil {
				registerParams.CallParams.Authorization = signatureResult.Signature
				sendRegister(registerParams)
			}
		}
	}
}

// Remember calls that where answered with 403
//var inviteCache = ttlcache.NewCache(time.Second * 10)
var authorizedCalls = ttlcache.NewCache(3 * time.Hour)
var cntSIPMessagesToProcess int32

func messageProcessor(n int) {

	for {
		msg := <-chanInboundMessage
		atomic.AddInt32(&cntSIPMessagesToProcess, 1)

		// https: //tools.ietf.org/html/rfc3261#section-16.6
		switch msg.Type {

		/** REQUEST HANDLING **/
		case MSG_TYPE_REQUEST:

			// CALL AUTHORIZATION
			if string(msg.RequestLine.Method) == "INVITE" {
				if tenantID == "open" { // NO AUTHORIZATION
					processRequest(msg)
				} else {
					var (
						isAuthorized      = false
						isRegisterPending = false
					)
					_, isAuthorized = authorizedCalls.Get(msg.Headers["Call-ID"][0])
					log.Debugf("RE-INVITE of authorized call %s", msg.Headers["Call-ID"][0])

					var authResult *AuthorizeResult
					if !isAuthorized {
						authResult = authorizeRequest(msg)
						if authResult != nil {
							isAuthorized = authResult.Authorized
							authorizedCalls.Set(msg.Headers["Call-ID"][0], "")
							if authResult.Register && len(authResult.SipUser) > 0 && msg.Headers["Authorization"] == nil {
								key := fmt.Sprintf("%s@%s", authResult.SipUser, msg.RequestLine.Host)
								if _, found := signatureTokens.Get(key); !found { // start periodic registration process (if not started yet)
									var to = siprocket.SipTo{}
									siprocket.ParseSipTo([]byte(msg.Headers["To"][0]), &to)
									isRegisterPending = true
									chanRegisterStart <- &RegisterParams{
										Account:        authResult.SipUser,
										Host:           string(to.Host),
										Port:           string(to.Port),
										SignatureToken: authResult.SignatureToken,
									}
								}
							}
						}
					}

					if !isAuthorized {
						send403(msg)
					} else if !isRegisterPending {
						processRequest(msg)
					}
				}

				atomic.AddInt32(&cntSIPMessagesToProcess, -1)
				continue
			}

			// Forward all other messages (ACK etc...)
			processRequest(msg)

		/** RESPONSE HANDLING **/
		case MSG_TYPE_RESPONSE:

			var statusCode = string(msg.RequestLine.StatusCode)

			// Handle 603 - Declined (Maybe register required)
			/*
				if statusCode == "603" {
					sendACK(msg)
					atomic.AddInt32(&cntSIPMessagesToProcess, -1)
					continue
				}
			*/

			var cseq = siprocket.SipCseq{}
			siprocket.ParseSipCseq([]byte(msg.Headers["CSeq"][0]), &cseq)

			// Handle register responses
			if string(cseq.Method) == "REGISTER" {
				var to = siprocket.SipTo{}
				siprocket.ParseSipTo([]byte(msg.Headers["To"][0]), &to)

				// handle Authentication
				if statusCode == "401" || statusCode == "407" {

					var auth = siprocket.SipProxyAuthenticate{}
					siprocket.ParseProxyAuthenticate([]byte(msg.Headers["WWW-Authenticate"][0]), &auth)

					var cSeq = strings.Split(msg.Headers["CSeq"][0], " ")[0]
					var (
						cSeqNum int
						err     error
					)
					if cSeqNum, err = strconv.Atoi(cSeq); err == nil {
						cSeqNum++
						cSeq = strconv.Itoa(cSeqNum)
					}

					var callID = msg.Headers["Call-ID"][0]
					var from = msg.Headers["From"][0]
					var via = strings.SplitN(msg.Headers["Via"][0], "received=", 2)[0] + "rport"

					chanRegisterChallenge <- &RegisterParams{
						Account: string(to.User),
						Host:    string(to.Host),
						Port:    string(to.Port),
						CallParams: &RegisterCallParams{
							CSeq:   cSeq,
							From:   from,
							Via:    via,
							CallID: callID,
						},
						Challenge: &RegisterChallenge{
							Realm:     auth.Realm,
							Nonce:     auth.Nonce,
							Opaque:    auth.Opaque,
							Algorithm: auth.Algorithm,
							QOP:       auth.Qop,
						},
					}
				} else if statusCode == "200" {
					// Re-Register after expires/2 seconds before expiration of registration
					var expires = 60
					var registerParams = RegisterParams{
						Account: string(to.User),
						Host:    string(to.Host),
						Port:    string(to.Port),
					}
					if msg.Headers["Min-Expires"] != nil {
						registerParams.Expires = msg.Headers["Min-Expires"][0]
						chanRegisterStart <- &registerParams
					} else {
						if msg.Headers["Expires"] != nil {
							expires, _ = strconv.Atoi(msg.Headers["Expires"][0])
						} else {
							var contact = siprocket.SipContact{}
							siprocket.ParseSipContact([]byte(msg.Headers["Contact"][0]), &contact)
							if len(contact.Expires) > 0 {
								expires, _ = strconv.Atoi(string(contact.Expires))
							}
						}
						var timeout = time.Duration(expires/2) * time.Second
						timer := time.NewTimer(timeout)
						go func() {
							<-timer.C
							chanRegisterStart <- &registerParams
						}()
					}
				} else {
					// delete signature tokens from cache on invalid REGISTER response (e.g. 404)
					account := string(to.User)
					host := string(to.Host)
					key := fmt.Sprintf("%s@%s", account, host)
					log.Infof("Received status code %s for REGISTER request --> Delete registration for account %s", statusCode, key)
					signatureTokens.Delete(key)
				}
			} else {
				processResponse(msg)
			}
		}

		atomic.AddInt32(&cntSIPMessagesToProcess, -1)
	}
}

type AuthorizeResult struct {
	Authorized     bool   `json:"authorized"`
	Register       bool   `json:"register"`
	SipUser        string `json:"sip_user"`
	SignatureToken string `json:"signature_token"`
}

func authorizeRequest(msg *SIPMessage) *AuthorizeResult {

	if authToken, ok := msg.Headers["X-Auth-Token"]; ok {
		msg.RemoveHeader("X-Auth-Token")
		var phoneNumber = string(msg.RequestLine.User)
		var authResult = getAuthorization(phoneNumber, authToken[0])
		return authResult
	}
	return nil
}

func getAuthorization(phoneNumber string, token string) *AuthorizeResult {

	url := BASE_URL + "/api/tenants/" + tenantID + "/sipproxy/authorize"

	var data = map[string]interface{}{
		"phone_number": phoneNumber,
	}

	var err error
	payload, err := json.Marshal(data)
	if err != nil {
		log.Errorf("%v\n", err.Error())
		return nil
	}

	var req *http.Request
	if req, err = http.NewRequest("POST", url, bytes.NewReader(payload)); err != nil {
		log.Errorf("%v\n", err.Error())
		return nil
	}
	req.Header.Set("Authorization", "Bearer "+token)
	req.Header.Set("Content-Type", "application/json")

	var result []byte
	var resp *http.Response
	for i := 0; i < 10; i++ {

		resp, err = http.DefaultClient.Do(req)

		if err == nil && resp.StatusCode == 403 {
			log.Errorf("%v\n", url+" - "+resp.Status)
			return nil
		}

		if err != nil || resp.StatusCode != http.StatusOK {
			timeout := 10 * time.Millisecond * time.Duration(math.Pow(2, float64(i)))
			if err != nil {
				log.Infof("[POST] %v | attempt: %v | error: %v | next try in %v ", url, i, err.Error(), timeout)
			} else {
				log.Infof("[POST] %v | attempt: %v | status: %v | next try in %v ", url, i, resp.Status, timeout)
			}
			time.Sleep(timeout)
			continue
		}

		defer resp.Body.Close()

		if result, err = ioutil.ReadAll(resp.Body); err != nil {
			log.Errorf("%v\n", err.Error())
			continue
		}

		var authResult AuthorizeResult
		if err = json.Unmarshal(result, &authResult); err != nil {
			log.Errorf("%v\n", err.Error())
			continue
		}
		return &authResult
	}

	if err != nil {
		log.Errorf("Authorization failed with error: %v\n", err.Error())
	}
	return nil
}

type SignatureData struct {
	Method    string `json:"method"`
	SipURI    string `json:"sipuri"`
	Realm     string `json:"realm"`
	Nonce     string `json:"nonce"`
	Opaque    string `json:"opaque"`
	Algorithm string `json:"algorithm"`
	QOP       string `json:"qop"`
	NC        int    `json:"nc"`
	Body      string `json:"body"`
}

type SignatureResult struct {
	Signature string `json:"signature"`
}

func CreateSignature(authData SignatureData, token string) *SignatureResult {

	//log.Debugf("create signature using token %v", token)

	url := BASE_URL + "/api/tenants/" + tenantID + "/sipproxy/signature/create"

	var err error
	payload, err := json.Marshal(authData)
	if err != nil {
		log.Errorf("%v\n", err.Error())
		return nil
	}

	var req *http.Request
	if req, err = http.NewRequest("POST", url, bytes.NewReader(payload)); err != nil {
		log.Errorf("%v\n", err.Error())
		return nil
	}
	req.Header.Set("Authorization", "Bearer "+token)
	req.Header.Set("Content-Type", "application/json")

	var result []byte
	var resp *http.Response
	for i := 0; i < 10; i++ {

		resp, err = http.DefaultClient.Do(req)

		if err == nil && resp.StatusCode == 403 {
			log.Errorf("%v\n", url+" - "+resp.Status)
			return nil
		}

		if err != nil || resp.StatusCode != http.StatusOK {
			timeout := 10 * time.Millisecond * time.Duration(math.Pow(2, float64(i)))
			if err != nil {
				log.Infof("[POST] %v | attempt: %v | error: %v | next try in %v ", url, i, err.Error(), timeout)
			} else {
				log.Infof("[POST] %v | attempt: %v | status: %v | next try in %v ", url, i, resp.Status, timeout)
			}
			time.Sleep(timeout)
			continue
		}

		defer resp.Body.Close()

		if result, err = ioutil.ReadAll(resp.Body); err != nil {
			log.Errorf("%v\n", err.Error())
			continue
		}

		var signatureResult SignatureResult
		if err = json.Unmarshal(result, &signatureResult); err != nil {
			log.Errorf("%v\n", err.Error())
			continue
		}

		return &signatureResult
	}

	if err != nil {
		log.Errorf("Create signature failed with error: %v\n", err.Error())
	}
	return nil
}

func processRequest(msg *SIPMessage) {

	// 1. Increment / Set Max-Forwards Header
	if maxForwards, ok := msg.Headers["Max-Forwards"]; ok {
		if val, err := strconv.Atoi(maxForwards[0]); err == nil {
			msg.Headers["Max-Forwards"][0] = strconv.Itoa(val - 1)
		}
	} else {
		msg.Headers["Max-Forwards"] = []string{"70"}
	}

	// 2. Add Record-Route Header (to stay in path)
	msg.AddHeader("Record-Route", "<sip:"+msg.IP+":"+port+";lr>")

	// 3. Remove top most Route header
	msg.RemoveHeader("Route")

	// 4. Add Via
	var branchToken = createBranchToken(msg)
	msg.Headers["Via"] = append([]string{"SIP/2.0/UDP " + msg.IP + ":" + port + ";rport;branch=" + branchToken}, msg.Headers["Via"]...)

	// 5. Remove X-Auth-Token Header
	msg.RemoveHeader("X-Auth-Token")

	//log.Debugf("process request: \n%s\n", msg.serialize())

	chanOutboundMessages <- msg
}

func processResponse(msg *SIPMessage) {

	// 1. Remove top most Via Header
	msg.RemoveHeader("Via")

	//log.Debugf("process response: %v", string(msg.serialize()))

	chanOutboundMessages <- msg
}

func createBranchToken(msg *SIPMessage) string {

	var via = siprocket.SipVia{}
	siprocket.ParseSipVia([]byte(msg.Headers["Via"][0]), &via)

	var cseq = siprocket.SipCseq{}
	siprocket.ParseSipCseq([]byte(msg.Headers["CSeq"][0]), &cseq)

	h := md5.New()
	io.WriteString(h, string(via.Branch)+string(via.Trans)+string(via.Host)+string(via.Port)+msg.Headers["Call-ID"][0]+string(cseq.Id))
	return "z9hG4bK" + hex.EncodeToString(h.Sum(nil))
}

func send403(sip *SIPMessage) {

	msg := SIPMessage{
		Type: MSG_TYPE_RESPONSE,
		RequestLine: siprocket.SipReq{
			StatusCode: []byte("403"),
			StatusDesc: []byte("Forbidden"),
		},
		HeaderNames: []string{"Via", "Max-Forwards", "From", "To", "Call-ID", "CSeq", "Content-Length"},
		Headers: map[string][]string{
			"Via":            sip.Headers["Via"],
			"Max-Forwards":   {"70"},
			"From":           sip.Headers["From"],
			"To":             sip.Headers["To"],
			"Call-ID":        sip.Headers["Call-ID"],
			"CSeq":           sip.Headers["CSeq"],
			"Content-Length": {"0"},
		},
	}

	log.Debugf("create 403:\n%v", string(msg.serialize()))

	chanOutboundMessages <- &msg
}

func sendACK(sip *SIPMessage) {

	var to = siprocket.SipTo{}
	siprocket.ParseSipTo([]byte(sip.Headers["To"][0]), &to)

	// Remove top most Via Header
	sip.RemoveHeader("Via")

	msg := SIPMessage{
		Type: MSG_TYPE_REQUEST,
		RequestLine: siprocket.SipReq{
			Method:  []byte("ACK"),
			UriType: to.UriType,
			User:    to.User,
			Host:    to.Host,
			Port:    to.Port,
		},
		HeaderNames: []string{"Via", "From", "To", "Call-ID", "CSeq", "Content-Length"},
		Headers: map[string][]string{
			"Via":            sip.Headers["Via"],
			"From":           sip.Headers["From"],
			"To":             sip.Headers["To"],
			"Call-ID":        sip.Headers["Call-ID"],
			"CSeq":           {strings.Split(sip.Headers["CSeq"][0], " ")[0] + " ACK"},
			"Content-Length": {"0"},
		},
	}
	chanOutboundMessages <- &msg
}

func sendRegister(params *RegisterParams) {

	var contact = fmt.Sprintf("<sip:%s@%s:%s;transport=UDP>", params.Account, params.Host, params.Port)

	var (
		callID        = string(RandASCIIBytes(32))
		cSeq          = "1"
		from          = fmt.Sprintf("%s;tag=%s", contact, RandASCIIBytes(8))
		via           = fmt.Sprintf("SIP/2.0/UDP %s;rport;branch=z9hG4bK%s", params.Host, RandASCIIBytes(8))
		authorization = ""
	)
	if params.CallParams != nil {
		cSeq = params.CallParams.CSeq
		from = params.CallParams.From
		via = params.CallParams.Via
		callID = params.CallParams.CallID
		authorization = params.CallParams.Authorization
	}

	var headers = map[string][]string{
		"Via":            {via},
		"Max-Forwards":   {"70"},
		"Contact":        {contact},
		"From":           {from},
		"To":             {contact},
		"Call-ID":        {callID},
		"CSeq":           {fmt.Sprintf("%s REGISTER", cSeq)},
		"Expires":        {"60"},
		"Content-Length": {"0"},
	}
	if len(params.Expires) > 0 {
		headers["Expires"] = []string{params.Expires}
	}
	if len(authorization) > 0 {
		headers["Authorization"] = []string{authorization}
	}
	var headerNames = make([]string, len(headers))
	i := 0
	for k := range headers {
		headerNames[i] = k
		i++
	}

	var msgRegister = SIPMessage{
		Type: MSG_TYPE_REQUEST,
		RequestLine: siprocket.SipReq{
			Method:  []byte("REGISTER"),
			UriType: "sip",
			Host:    []byte(params.Host),
			Port:    []byte(params.Port),
		},
		HeaderNames: headerNames,
		Headers:     headers,
	}

	chanOutboundMessages <- &msgRegister
}

const letterBytes = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandASCIIBytes(n int) []byte {
	output := make([]byte, n)
	// We will take n bytes, one byte for each character of output.
	randomness := make([]byte, n)
	// read all random
	_, err := rand.Read(randomness)
	if err != nil {
		panic(err)
	}
	l := len(letterBytes)
	// fill output
	for pos := range output {
		// get random item
		random := uint8(randomness[pos])
		// random % 64
		randomPos := random % uint8(l)
		// put into output
		output[pos] = letterBytes[randomPos]
	}
	return output
}

type MSG_TYPE int

const (
	MSG_TYPE_REQUEST  = 0
	MSG_TYPE_RESPONSE = 1
)

var compactHeaders = map[string]string{
	"a": "Accept-Contact",
	"b": "Referred-By",
	"c": "Content-Type",
	"e": "Content-Encoding",
	"f": "From",
	"i": "Call-ID",
	"k": "Supported",
	"l": "Content-Length",
	"m": "Contact",
	"o": "Event",
	"r": "Refer-To",
	"s": "Subject",
	"t": "To",
	"u": "Allow-Events",
	"v": "Via",
}

type SIPMessage struct {
	IP          string
	Type        MSG_TYPE
	RequestLine siprocket.SipReq
	HeaderNames []string // to remember the order of the headers
	Headers     map[string][]string
	Body        []byte
}

func (m *SIPMessage) AddHeader(name string, value string) {

	// replace compact header
	if h, ok := compactHeaders[name]; ok {
		name = h
	}

	if m.Headers[name] == nil {
		m.HeaderNames = append(m.HeaderNames, name)
	}
	m.Headers[name] = append(m.Headers[name], value)
}

/** removes the top most header with the given name **/
func (m *SIPMessage) RemoveHeader(name string) {

	if len(m.Headers[name]) == 0 {
		return
	}
	m.Headers[name] = m.Headers[name][1:]
}

func (m *SIPMessage) serialize() []byte {

	var b bytes.Buffer
	var w = textproto.NewWriter(bufio.NewWriter(&b))

	// start-line
	if m.Type == MSG_TYPE_REQUEST {
		if m.RequestLine.User != nil {
			w.PrintfLine("%s %s:%s@%s:%s SIP/2.0", m.RequestLine.Method, m.RequestLine.UriType, m.RequestLine.User, m.RequestLine.Host, m.RequestLine.Port)
		} else {
			w.PrintfLine("%s %s:%s:%s SIP/2.0", m.RequestLine.Method, m.RequestLine.UriType, m.RequestLine.Host, m.RequestLine.Port)
		}

	} else if m.Type == MSG_TYPE_RESPONSE {
		w.PrintfLine("SIP/2.0 %s %s", m.RequestLine.StatusCode, m.RequestLine.StatusDesc)
	}

	// headers
	for _, hname := range m.HeaderNames {
		var hvalues = m.Headers[hname]
		for _, hval := range hvalues {
			w.PrintfLine("%s: %s", hname, hval)
		}
	}

	w.PrintfLine("") // CRLF
	return append(b.Bytes(), m.Body...)
}

func parseMessage(data []byte) *SIPMessage {

	var msg SIPMessage

	scanner := bufio.NewScanner(bytes.NewReader(data))

	var state = 0
	for scanner.Scan() {
		line := scanner.Text()

		switch state {

		case 0:
			var reqLine = siprocket.SipReq{}
			siprocket.ParseSipReq([]byte(line), &reqLine)

			if strings.HasPrefix(string(line), "SIP/2.0") {
				msg = SIPMessage{
					Type: MSG_TYPE_RESPONSE,
				}
			} else if strings.HasSuffix(string(line), "SIP/2.0") {

				msg = SIPMessage{
					Type: MSG_TYPE_REQUEST,
				}
			} else {
				return nil
			}

			msg.RequestLine = reqLine
			msg.Headers = make(map[string][]string)
			if len(publicIP) > 0 {
				msg.IP = publicIP
				log.Debugf("use IP from config: %v", publicIP)
			}
			/* LocalIP is same as listenIP of proxy --> no need to be fetched from UDP Packet
			else {
				msg.IP = strings.Split(udpPacket.LocalIP, ":")[0]
				log.Debugf("use IP from packet: %v", msg.IP)
			}
			*/

			state++

		case 1:
			if len(line) == 0 {
				state++
				continue
			} else if line[0] == 0 { // 0 indicates end of message
				break
			}

			log.Debugf("Line: %v (%v)", line, len(line))
			keyVal := strings.SplitN(string(line), ":", 2)
			if len(keyVal) >= 2 {
				msg.AddHeader(strings.TrimSpace(keyVal[0]), strings.TrimSpace(keyVal[1]))
			}

		case 2:
			if len(line) == 0 || line[0] == 0 { // 0 indicates end of message
				break
			}
			msg.Body = append(msg.Body, []byte(line)...)
			msg.Body = append(msg.Body, []byte("\r\n")...)
		}
	}
	return &msg
}
