module bitbucket.org/modima/sipproxy

go 1.18

require (
	bitbucket.org/modima/sipProxy v0.0.0-20230220151511-d38670d25c1d
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
)
