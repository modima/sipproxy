package siprocket

import (
	"strings"
)

/*
RFC3261 - https://tools.ietf.org/html/rfc3261#page-174

20.27 Proxy-Authenticate

   A Proxy-Authenticate header field value contains an authentication
   challenge.

   The use of this header field is defined in [H14.33].  See Section
   22.3 for further details on its usage.

   Example:

      Proxy-Authenticate: Digest realm="atlanta.com",
       domain="sip:ss1.carrier.com", qop="auth",
       nonce="f84f1cec41e6cbe5aea9c8e88d359",
       opaque="", stale=FALSE, algorithm=MD5
*/

type SipProxyAuthenticate struct {
	Realm     string
	Domain    string
	Qop       string
	Nonce     string
	Opaque    string
	Stale     string
	Algorithm string
	Src       []byte // Full source if needed
}

func ParseProxyAuthenticate(v []byte, out *SipProxyAuthenticate) {

	var key string
	idxValStart := 0
	pos := 0

	// Keep the source line if needed
	if keep_src {
		out.Src = v
	}

	//fmt.Printf("%s\n", v)

	// Loop through the bytes making up the line
	for pos < len(v) {

		if v[pos] == ' ' {
			pos++
			idxValStart = pos
			continue
		} else if v[pos] == '=' {
			key = getString(v, idxValStart, pos)
			//fmt.Println("key: " + string(key))
			pos++
			idxValStart = pos
			continue
		} else if v[pos] == ',' || pos == len(v)-1 {

			switch strings.ToLower(key) {
			case "realm":
				out.Realm = strings.ReplaceAll(getString(v, idxValStart, pos), "\"", "")
			case "domain":
				out.Domain = strings.ReplaceAll(getString(v, idxValStart, pos), "\"", "")
			case "qop":
				out.Qop = strings.ReplaceAll(getString(v, idxValStart, pos), "\"", "")
			case "nonce":
				out.Nonce = strings.ReplaceAll(getString(v, idxValStart, pos), "\"", "")
			case "opaque":
				out.Opaque = strings.ReplaceAll(getString(v, idxValStart, pos), "\"", "")
			case "stale":
				out.Stale = strings.ReplaceAll(getString(v, idxValStart, pos), "\"", "")
			case "algorithm":
				out.Algorithm = strings.ReplaceAll(getString(v, idxValStart, pos), "\"", "")
			}
			pos++
			idxValStart = pos
			continue
		}
		pos++
	}
}
