package siprocket

/*

RFC 3261 - https://www.ietf.org/rfc/rfc3261.txt - 20.34 Route

   The Route header field is used to force routing for a request through
   the listed set of proxies.

Examples:

    Route: <sip:bigbox3.site3.atlanta.com;lr>,
           <sip:server10.biloxi.com;lr>
*/

type SipRoute struct {
	UriType string // Type of URI sip, sips, tel etc
	Name    []byte // Named portion of URI
	User    []byte // User part
	Host    []byte // Host part
	Port    []byte // Port number
	Tran    []byte // Transport
	Qval    []byte // Q Value
	Expires []byte // Expires
	Src     []byte // Full source if needed
}

func ParseSipRoute(v []byte, out *SipRoute) {

	pos := 0
	state := FIELD_BASE

	// Init the output area
	out.UriType = ""
	out.Name = nil
	out.User = nil
	out.Host = nil
	out.Port = nil
	out.Tran = nil
	out.Qval = nil
	out.Expires = nil
	out.Src = nil

	// Keep the source line if needed
	if keep_src {
		out.Src = v
	}

	// Loop through the bytes making up the line
	for pos < len(v) {
		// FSM
		//fmt.Println("POS:", pos, "CHR:", string(v[pos]), "STATE:", state)
		switch state {
		case FIELD_BASE:
			if v[pos] == '"' && out.UriType == "" {
				state = FIELD_NAMEQ
				pos++
				continue
			}
			if v[pos] != ' ' {
				// Not a space so check for uri types
				if getString(v, pos, pos+4) == "sip:" {
					state = FIELD_HOST
					pos = pos + 4
					out.UriType = "sip"
					continue
				}
				if getString(v, pos, pos+5) == "sips:" {
					state = FIELD_HOST
					pos = pos + 5
					out.UriType = "sips"
					continue
				}
				if getString(v, pos, pos+4) == "tel:" {
					state = FIELD_HOST
					pos = pos + 4
					out.UriType = "tel"
					continue
				}
				// Look for other identifiers and ignore
				if v[pos] == '=' {
					state = FIELD_IGNORE
					pos = pos + 1
					continue
				}
				// Check for other chrs
				if v[pos] != '<' && v[pos] != '>' && v[pos] != ';' && out.UriType == "" {
					state = FIELD_NAME
					continue
				}
			}

		case FIELD_NAMEQ:
			if v[pos] == '"' {
				state = FIELD_BASE
				pos++
				continue
			}
			out.Name = append(out.Name, v[pos])

		case FIELD_NAME:
			if v[pos] == '<' || v[pos] == ' ' {
				state = FIELD_BASE
				pos++
				continue
			}
			out.Name = append(out.Name, v[pos])

		case FIELD_HOST:
			if v[pos] == ':' {
				state = FIELD_PORT
				pos++
				continue
			}
			if v[pos] == ';' || v[pos] == '>' {
				state = FIELD_BASE
				pos++
				continue
			}
			out.Host = append(out.Host, v[pos])

		case FIELD_PORT:
			if v[pos] == ';' || v[pos] == '>' || v[pos] == ' ' {
				state = FIELD_BASE
				pos++
				continue
			}
			out.Port = append(out.Port, v[pos])

		case FIELD_TRAN:
			if v[pos] == ';' || v[pos] == '>' || v[pos] == ' ' {
				state = FIELD_BASE
				pos++
				continue
			}
			out.Tran = append(out.Tran, v[pos])

		case FIELD_Q:
			if v[pos] == ';' || v[pos] == '>' || v[pos] == ' ' {
				state = FIELD_BASE
				pos++
				continue
			}
			out.Qval = append(out.Qval, v[pos])

		case FIELD_EXPIRES:
			if v[pos] == ';' || v[pos] == '>' || v[pos] == ' ' {
				state = FIELD_BASE
				pos++
				continue
			}
			out.Expires = append(out.Expires, v[pos])

		case FIELD_IGNORE:
			if v[pos] == ';' || v[pos] == '>' {
				state = FIELD_BASE
				pos++
				continue
			}

		}
		pos++
	}
}
