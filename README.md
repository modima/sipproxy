## Description
---------------------------------------------------
* Enables the use of Dialfire in conjunction with a SIP trunk that only supports IP based authentication.
* In addition, the proxy can dynamically register with the SIP Trunk if required.

## Start-up
---------------------------------------------------
```C++
sipProxy -ip {public ip of your machine} -ld {directory for logfiles} -t {operation mode}
```

## Operation modes
--------------
* {your tenant id} ... use your tenant id to only allow calls from your Dialfire tenant (recommended)
* all ... accept all calls from Dialfire
* open ... accept all calls from anywhere WITHOUT authorization (not recommended)

## Full list of command line options
--------------
```C++
sipProxy --help
```